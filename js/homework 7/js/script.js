'use strict';

function showList(list) {

    function arrayEntry(root, arr) {
        let ul = document.createElement('ul');
        let li;

        root.appendChild(ul);

        arr.forEach(function(item) {
            if (Array.isArray(item)) {
                Array(li, item);
                return;
            }

            li = document.createElement('li');
            li.appendChild(document.createTextNode(item));
            ul.appendChild(li);
        });
    }

    let div = document.getElementById('myList');

    arrayEntry(div, list);



}

showList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);