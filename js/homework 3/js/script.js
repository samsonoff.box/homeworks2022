"use strict";

let a = +prompt("Please input number 1");
let b = +prompt("Please input number 2");
let c = prompt("Please input mathematical sign");

while (isNaN(a) || !a || isNaN(b) || !b) {
    a = +prompt("Please input number 1");
    b = +prompt("Please input number 2");
    c = prompt("Please input mathematical sign");
}

switch (c) {
    case "+":
        console.log(a + b);
        break;
    case "-":
        console.log(a - b);
        break;
    case "*":
        console.log(a * b);
        break;
    case "/":
        console.log(a / b);
        break;
}
