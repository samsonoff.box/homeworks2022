Теоретические вопросы:

1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

Ответ: Оператор var использовался в старом синтаксисе (до ES6), в последнем издании данный оператор был заменен на let и const.
Переменная, объявленная с помощью var, могла быть переобъявлена по новой. Использование let позволяет нам обновлять переменную,
но уже без повторного объявления. Третий же вариант, объявление переменной через const, не позволяет нам впринципе обновлять
эту переменную в дальнейшем, это уже константное значение, которое не изменится.

2. Почему объявлять переменную через var считается плохим тоном?

Ответ: Первая причина - это то, что последнее издание ESMA-Script не подразумеват использование данного оператора.
Ему на замену пришли новые, а это значит, что лучше не использовать синтаксис, который уже не актуален.
Вторая причина - переменные var не имеют блочной области видимости, они ограничены, как минимум, телом функции.