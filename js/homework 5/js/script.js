"use strict";

function CreateNewUser() {
        this.firstName = prompt('Please indicate your name');
        this.lastName = prompt('Please indicate your last name');
        this.birthday = new Date(prompt("Please indicate your date of birth in following format: dd.mm.yyyy", "dd.mm.yyyy"));
}

CreateNewUser.prototype.getLogin = function () {
    let newName = this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    console.log(newName);
}

CreateNewUser.prototype.getPassword = function () {
    let pass = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    console.log(pass);
}


let newUser = new CreateNewUser();
newUser.getLogin();
newUser.getPassword();