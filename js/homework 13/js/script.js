"use strict";

let page = document.querySelector('.page');
let themeButton = document.querySelector('.theme-button');
themeButton.onclick = function() {
    // if (localStorage.getItem('theme')!==null) {
    //     let color = localStorage.getItem('theme');
    //     page.classList.toggle(color);
    // }

    page.classList.toggle('light-theme');
    page.classList.toggle('dark-theme');
    // localStorage.setItem('theme', 'light-theme');
    // localStorage.setItem('theme', 'dark-theme');
};
